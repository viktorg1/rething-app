import React from 'react';
import {Col, Row, Image} from 'react-bootstrap';
import biglogo from '../../assets/images/biglogo.png';
import './Footer.css';

const Footer = (props) => {
    return (
        <div style={{background:'#000000', position:'relative'}} id="contact">
        <div className="p-5">

          <Row>
            <Col md='4'>
              <Image src={biglogo} className="img-fluid"/>
              <p className="Heading text-secondary text-monospace text-center pt-3">re<span style={{color:'#FCD635'}}>\</span>thing</p>
            </Col>
            <Col md='2' className="mt-5">
              <p className="FooterHeading">
                Contact
              </p>
              <p className="FooterParagraph">
                <p>info@rething.com</p> 
                <p>contact@rething.com</p> 
                <p>389 2 123 45 67 </p>
              </p>
            </Col>

            <Col md='2' className="mt-5">
              <p className="FooterHeading">
                Visit
              </p>
              <p className="FooterParagraph">
                <p>Bul. Kiril i Metodij, 18
                  Skopje, Macedonia</p> 
              </p>
            </Col>
            
            <Col md='2' className="mt-5">
              <p className="FooterHeading">
                Social
              </p>
              <p className="FooterParagraph">
                <p><a href="https://linkedin.com/">LinkedIn</a> </p>
                <p><a>Facebook</a> </p>
                <p><a>Instagram</a></p>
              </p>
            </Col>
            
            <Col md='2'>
              <p className="Copyright">© 2021 Re/thing. All Rights Reserved</p>
            
            </Col>

          </Row>
          
        </div>
      </div>
    );
}

export default Footer;