import React from 'react';
import {Image, Col, Row} from 'react-bootstrap';
import male from '../../assets/images/male.png';
import female from '../../assets/images/female.png';
import grayBlock from '../../assets/images/grayBlock.png';
import orangeBlock from '../../assets/images/orangeBlock.png';
import './Management.css';

const Management = (props) => {
    return (
        <div className="Management">
            <div id="management" className="my-5">
          <p className="Heading" style={{color:'black'}}> 
            What we do?
          </p> 
          <p className="Paragraph">
            We understand our audience! Our designs, from minimalistic to detailed, speak to a wider audience. We target their needs and solve its problems to the core.
          </p>
          <p className="Paragraph">
            We deliver creations we strongly believe in! We tell a story with our products offering a brand new soul to the client's idea. 
          </p>
          <p className="Paragraph">
            We don't work for a client, we work with them right from the early stages of each project, up to its last moments before launching. We work with clients who are as passionate about their ideas as much as we are about what we do!        
          </p> 
        </div>

        <div id="team" className="my-5">
          <p className="Heading" style={{color:'black'}}> 
            The team that re\design the future
          </p> 
          <p className="Paragraph">
          ReThing is a small company with its roots in Skopje, Macedonia. We are a group of designers, developers and smart-thinkers with one important goal; To help your business launch and grow their greatest ideas in the shortest period of time.We have a strong and well organised team with expertise and knowledge in app & web development, branding and design; ensuring that all of the requirements for the software development life-cycle will be met. (all the while providing a tailored approach)
          </p>

          <div id="TeamFlexbox">
            <Row>

              <Col md='2' className="FlexBox">
                <figure className="position-relative">
                
                  <Image className="img-fluid" src={female}/>
                  <figcaption className="text-center">
                    <span className="font-weight-bold">Dorotea</span><br />
                    Digital Marketing Intern
                  </figcaption>
                </figure>


              </Col>
              <Col md='2' className="FlexBox">
              <figure className="position-relative">
                
                <Image className="img-fluid" src={female}/>
                <figcaption className="text-center">
                  <span className="font-weight-bold">Katerina</span><br />
                  Project Manager
                </figcaption>
              </figure>

              </Col>
              <Col md='2' className="FlexBox">
              <figure className="position-relative">
                
                <Image className="img-fluid" src={male} />
                <figcaption className="text-center">
                  <span className="font-weight-bold">Martin</span><br />
                  QA Tester
                </figcaption>
              </figure>

              </Col>
              <Col md='2' className="FlexBox">
              <figure className="position-relative">

                <Image className="img-fluid" src={female}/>
                <figcaption className="text-center">
                  <span className="font-weight-bold">Dea</span><br />
                  Graphic Desinger
                </figcaption>
              </figure>
                
              </Col>
              <Col md='2' className="FlexBox">
              <figure className="position-relative">
                
                <Image className="img-fluid" src={female}/>
                <figcaption className="text-center">
                  <span className="font-weight-bold">Ivana</span><br />
                  
                </figcaption>
              </figure>

              </Col>
              <Col md='2' className="FlexBox">
              
              <figure className="position-relative">
                
                <Image className="img-fluid" src={female}/>
                <figcaption className="text-center">
                  <span className="font-weight-bold">Dushica</span><br />
                  UX/UI Design Intern
                </figcaption>
              </figure>
                
              </Col>

            </Row>
            <Row>

              <Col md='2' className="FlexBox">
              
              <figure className="position-relative">
                
                <Image className="img-fluid" src={female}/>
                <figcaption className="text-center">
                  <span className="font-weight-bold">Tea</span><br />
                  Graphic Design Intern
                </figcaption>
              </figure>

              </Col>
              <Col md='2' className="FlexBox">
              
              <figure className="position-relative">
                
                <Image className="img-fluid" src={male}/>
                <figcaption className="text-center">
                  <span className="font-weight-bold">Dimitar</span><br />
                  Project Manager
                </figcaption>
              </figure>

              </Col>
              <Col md='2' className="FlexBox">
              
              <figure className="position-relative">
                
                <Image className="img-fluid" src={female}/>
                <figcaption className="text-center">
                  <span className="font-weight-bold">Mayda</span><br />
                  UX/UI Design Intern
                </figcaption>
              </figure>

              </Col>
              <Col md='2' className="FlexBox">
              
              <figure className="position-relative">
                
                <Image className="img-fluid" src={male}/>
                <figcaption className="text-center">
                  <span className="font-weight-bold">Slobodan</span><br />
                  Front-end development intern
                </figcaption>
              </figure>
                
              </Col>
              <Col md='2' className="FlexBox">
              
              <figure className="position-relative">
                
                <Image className="img-fluid" src={female}/>
                <figcaption className="text-center">
                  <span className="font-weight-bold">Bisera</span><br />
                  Graphic Designer
                </figcaption>
              </figure>
                
              </Col>
              <Col md='2' className="FlexBox">
              
              <figure className="position-relative">
                
                <Image className="img-fluid" src={male}/>
                <figcaption className="text-center">
                  <span className="font-weight-bold">Filip</span><br />
                  Front-end Development Intern
                </figcaption>
              </figure>
                
              </Col>
              
            </Row>
            <Row>
              
              <Col md='2' className="FlexBox">
              
              <figure className="position-relative">
                
                <Image className="img-fluid" src={male}/>
                <figcaption className="text-center">
                  <span className="font-weight-bold">Dejan</span><br />
                  Front-end Developer
                </figcaption>
              </figure>
                
              </Col>
              <Col md='2' className="FlexBox">
              
              <Image className="img-fluid" src={grayBlock} />
              
              </Col>
              <Col md='8' className="FlexBox">
                
                <Image className="img-fluid" style={{height:'160px'}} src={orangeBlock} />
                
              </Col>
            
            </Row>
            
          </div>
        </div>
        </div>
    );
}

export default Management;