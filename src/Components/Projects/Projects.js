import React from 'react';
import {Container, Col, Row, Image} from 'react-bootstrap';
import boksy from '../../assets/images/boksy.png';
import fitr from '../../assets/images/fitr.png';
import mcg from '../../assets/images/mcg.png';
import stabenfeldt from '../../assets/images/stabenfeldt.png';

const Projects = (props) => {
    return (
        <div style={{background:'#000000'}} id="projects">
        <Container className="py-5">
          <p className="Heading" style={{color:'white'}}> 
              Projects (?)
          </p> 
          <Row>
          <Col md='6'>
            <Image src={boksy} className="img-fluid"/>
          </Col>
          <Col md='6'>
          <Image src={stabenfeldt} className="img-fluid"/>
          </Col>
          </Row>
          <Row>
          <Col md='6'>
            <Image src={fitr} className="img-fluid"/>
          </Col>
          <Col md='6'>
          <Image src={mcg} className="img-fluid"/>
          </Col>
          </Row>
          
        </Container>
      </div>
    );
}

export default Projects;