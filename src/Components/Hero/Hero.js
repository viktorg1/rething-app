import React from 'react';
import './Hero.css';

const Hero = (props) => {
    return (
        <div className="Hero">
            <header id="home" className="Header">
                <p className="HeaderText">re\<span style={{color:"white"}}>thing</span></p>
            </header>
        </div>
    );
}

export default Hero;