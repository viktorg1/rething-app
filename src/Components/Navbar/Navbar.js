import React from "react";
import {Navbar, Nav, Container, Image} from 'react-bootstrap';
import logo from '../../assets/images/logo.png';
import './Navbar.css';

const NavBar = (props) => {
    return (
        
        <div className="NavBar">
            <Navbar id="navbar" style={{background:'#FCD635'}} className="fixed-top" expand="lg">
                <Container fluid>
                <Navbar.Brand href="#home"> <Image src={logo} /> </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ml-auto">
                    <Nav.Link href="#aboutUs">Who We Are</Nav.Link>
                    <Nav.Link href="#contact">Contact</Nav.Link>
                    <Nav.Link href="#projects">Projects</Nav.Link>
                    <Nav.Link href="#careers">Careers</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>


    );
}

export default NavBar;