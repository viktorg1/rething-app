import React from 'react';
import {Container, Row, Col, Image} from 'react-bootstrap';
import './Services.css';

const Services = (props) => {
    return (
        <div className="Services">
            <div style={{background:'#000000'}} id="services">
                <Container className="py-5">
                <p className="Heading" style={{color:'white'}}> 
                    What We Do (?)
                </p> 
                <Row>
                    <Col md="1"></Col>
                    <Col md='5' className='bg-white my-3 py-4'>
                        <Row>
                            <Col md="2">
                                <Image src="" alt="picture" />
                            </Col>
                            <Col md="10">
                                <p className="ServicesHeading">
                                    Web
                                </p>
                                <p className="ServicesParagraph">
                                    We work meticulously on creating a design that successfully translates your product’s message to its audience via a website. Our focus is always on captivating the users’ attention with engaging and expressive content.
                                </p>
                            </Col>
                        </Row>
                    </Col>
                    <Col md="1"></Col>
                    <Col md='5' className='bg-white my-3 py-4'>
                        <Row>
                            <Col md="2">
                                <Image src="" alt="picture" />
                            </Col>
                            <Col md="10">
                                <p className="ServicesHeading">
                                    App
                                </p>
                                <p className="ServicesParagraph">
                                    Carefully crafted user experience for both iOS and Android mobile applications. High quality, custom mobile app services is what we’re all about, while our end goal is always apps that are intuitive, simple and of course engaging and accessible. Expressive conte
                                </p>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Row>
                    <Col md="1"></Col>
                    <Col md='5' className='bg-white my-3 py-4'>
                        <Row>
                            <Col md="2">
                            <Image src="" alt="picture" />
                            </Col>
                            <Col md="10">
                                <p className="ServicesHeading">
                                    Custom digital content
                                </p>
                                <p className="ServicesParagraph">
                                Working on the visual design and brand language is a necessity, which is used to effectively connect you with your target audience.

                                </p>
                            </Col>
                        </Row>
                    </Col>
                    <Col md="1"></Col>
                    <Col md='5' className='bg-white my-3 py-4'>
                        <Row>
                            <Col md="2">
                                <Image src="" alt="picture" />
                            </Col>
                            <Col md="10">
                                <p className="ServicesHeading">
                                    Marketing
                                </p>
                                <p className="ServicesParagraph">
                                    Creation of a marketing strategy used to share your message through the use of content and the proper marketing channels. 
                                </p>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                
                </Container>
            </div>
        </div>
    );
}

export default Services;