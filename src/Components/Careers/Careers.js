import React from 'react';
import {Container, Accordion, Row, Col, Button, Image} from 'react-bootstrap';
import upload from '../../assets/images/upload.png';

const Careers = (props) => {
    return (
        <div className="Careers">
             <div id="careers" className="my-5">
        <Container>

        <p className="Heading">
          Re\think your future
        </p>
        <p className="Paragraph">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod te
        </p>
        <Accordion flush className="my-5 py-5">
          <Accordion.Item eventKey="0">
            <Accordion.Header>Full Stack Developer</Accordion.Header>
            <Accordion.Body className="Paragraph">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit e sse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
            <p>
            
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 

            </p>
            <ul>
              <li>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,</li> 
              <li>Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi </li>
              <li>Nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet</li>
              <li>Consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt </li>
              <li>Ut labore et dolore magnam aliquam quaerat voluptatem.</li>

            </ul>


            <p>
              Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? 
            </p>
            <Row>
              <Col md="10"><Image src={upload} style={{width:'45px'}}/> <span className="text-secondary text-decoration-underline pl-2 pt-2">Upload File</span></Col>
              <Col md="2">
                <Button style={{background:'#FCD635'}} size="lg" className="btn-block">\apply</Button>{' '}
              </Col>
            </Row>
            </Accordion.Body>
          </Accordion.Item>
          <Accordion.Item eventKey="1">
            <Accordion.Header>Digital Marketing Specialist</Accordion.Header>
            <Accordion.Body className="Paragraph">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit e sse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            </p>
            <p>
            
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 

            </p>
            <ul>
              <li>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,</li> 
              <li>Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi </li>
              <li>Nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet</li>
              <li>Consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt </li>
              <li>Ut labore et dolore magnam aliquam quaerat voluptatem.</li>

            </ul>


            <p>
              Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? 
            </p>
            <Row>
              <Col md="10"><Image src={upload} style={{width:'45px'}}/> <span className="text-secondary text-decoration-underline pl-2 pt-2">Upload File</span></Col>
              <Col md="2">
                <Button style={{background:'#FCD635'}} size="lg" className="btn-block">\apply</Button>{' '}
              </Col>
            </Row>
            </Accordion.Body>
          </Accordion.Item>
        </Accordion>
        </Container>
      </div>
        </div>
    );
}

export default Careers;