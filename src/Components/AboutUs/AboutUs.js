import React from "react";
import './AboutUs.css';

const AboutUs = (props) => {
    return (
      <div className="AboutUs">
        <div id="aboutUs" className="my-5">
          <p className="Heading" style={{color:'black'}}> 
            We reform ideas and re\shape them for the future
          </p> 
          <p className="Paragraph">
            We are a creative space that makes digital transformation of your ideas. We can reform your genius ideas into the digital world. Handling everything from idea, to concept, to design, to end product. We strongly believe in building products for the future while making it a great experience for its users. 
          </p> 
          <p className="Paragraph">We do not reinvent the wheel, we just roll it on a smoother road.  </p>
        </div>
      </div>  
    );
}

export default AboutUs;