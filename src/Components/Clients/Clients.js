import React from 'react';
import {Container, Form, Button, Row, Col, Image} from 'react-bootstrap';
import './Clients.css';
import upload from '../../assets/images/upload.png';

const Clients = (props) => {
    return (
        <div className="Clients text-left">
            <Container className="my-5 py-5">
                <p className="Heading">
                    It’s time to re\thing your product?
                </p>
                <p className="ClientsParagraph">
                    Tell us more about your ideas
                </p>
                <Form>
                    <Form.Group className="mb-3" controlId="formName">
                        <Form.Control type="text" placeholder="What is your name?" style={{borderWidth:'0px', borderBottomWidth:'1px'}} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formEmail">
                        <Form.Control type="email" placeholder="Enter your mail address" style={{borderWidth:'0px', borderBottomWidth:'1px'}} />
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="formIdea">
                        <Form.Control type="text" placeholder="Share your idea!" style={{borderWidth:'0px', borderBottomWidth:'1px'}} />
                    </Form.Group>
                    <Row className="mt-5">
                        <Col md="10"><Image src={upload} style={{width:'45px'}}/> <span className="text-secondary text-decoration-underline pl-2 pt-2">Upload File</span></Col>
                        <Col md="2">
                            <Button style={{background:'#FCD635'}} size="lg" className="btn-block">\apply</Button>{' '}
                        </Col>
                    </Row>
                </Form>
            </Container>
        </div> 
    );
}

export default Clients;