import './App.css';
import {Container} from 'react-bootstrap';

import Navbar from './Components/Navbar/Navbar';
import AboutUs from './Components/AboutUs/AboutUs';
import Management from './Components/Management/Management';
import Hero from './Components/Hero/Hero';
import Projects from './Components/Projects/Projects';
import Careers from './Components/Careers/Careers';
import Footer from './Components/Footer/Footer';
import Services from './Components/Services/Services';
import Clients from './Components/Clients/Clients';

function App() {
  return (
    <div className="App">
      <Navbar />
      <Hero />

      <Container data-spy="scroll" data-target="#navbar" data-offset="0">

        <AboutUs />
        
        <Management />

      </Container>

      <Services />

      <Clients />

      <Projects />

      <Careers />

      <Footer />
    </div>
  );
}

export default App;
